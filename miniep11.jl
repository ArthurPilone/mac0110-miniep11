#---------------------------------------------------------------
#MAC 0110 MiniEP 11  [7/2020]
#Por Arthur Pilone NUSP 11795450
#---------------------------------------------------------------

#Função auxiliar: prepara string para utilização na função principal

function tratarTexto(entrada)
    entrada = lowercase(entrada)
    saida = ""
    for caractere in entrada
        if caractere == '!' || caractere == '?' || caractere == '.' || caractere == ',' || caractere == '/' || caractere == '*' || caractere == '"' || caractere == '-' || caractere == ' '
            continue
        elseif caractere == 'á' || caractere == 'à' || caractere == 'ã'
            saida *= 'a'
        elseif caractere == 'é' || caractere == 'è' || caractere == 'ê'
            saida *= 'e'
        elseif caractere == 'ô' || caractere == 'ó'
            saida *= 'o'
        else
            saida *= caractere
        end
    end
    return saida
end

function palindromo(entrada)
    entrada = tratarTexto(entrada)

    saida = true

    iterador1 = firstindex(entrada)
    iterador2 = lastindex(entrada)

    while iterador1 <= iterador2
        if entrada[iterador1] != entrada[iterador2]
            return false
        end
        iterador1 = nextind(entrada, iterador1)
        iterador2 = prevind(entrada, iterador2)
    end

    return saida
end

using Test

function testarFuncao()
    @test palindromo("")
    @test !palindromo("Robert")
    @test palindromo("Ama")
    @test palindromo("ovO")
    @test !palindromo("Cachorro")
    @test !palindromo("MiniEP11")
    @test !palindromo("Olhe lá!")
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!")
    @test palindromo("A,lá.")
    @test palindromo("A mãe te ama.")
    @test palindromo("Roma me tem amor.")
    @test palindromo("A Rita, sobre vovô, verbos atira.")
    @test palindromo("Olé! Maracujá, caju, caramelo!")
    println("Fim dos testes")
end

#testarFuncao()
